#Account Service

An API that will provide services in CREATING and VIEWING USER and ACCOUNTS. The Service contains some business logic
such as:

1. ensuring that USERS will have a unique email address.
2. ACCOUNT will prohibit to be created if the total monthly earning is GREATER THAN 1000
3. SALARY and EXPENSES should be a POSITIVE NUMBER
4. Save the information in POSTGRES Database

##Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and 
testing purposes.

##Prerequisites

Depending on your purpose for this project, you may require the following below:

###Basic

1. A Laptop
2. Internet Connection

#### Testing Only

1. Docker

    MAC OS version 18.09.2 or later
    ```
    https://docs.docker.com/docker-for-mac/install/
    ```
    
    WINDOWS OS
    ```
    https://docs.docker.com/docker-for-windows/install/
    ```

2. Git

    ```
    https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
    ```

3. Postman (recommended) but Swagger is provided

    ```
    https://www.getpostman.com/downloads/
    ```

#### Development

1. Maven version 3.6.1

    ```
    https://maven.apache.org/download.cgi
    ```

2. Java 11.0.3 or later

    ```
    https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html
    ```

3. Java IDE of choice

    Intellij
    ```
    https://www.jetbrains.com/idea/
    ```

    Eclipse
    ```
    https://www.eclipse.org
    ```

## Installing

1. Open a Terminal or Git Bash from you computer and navigate to you where you want to save the application

    ```
    i.e. cd ~/Documents/Workspace/
    ```

2. clone the repository in bitbucket

    ```
    git clone https://marktimothytabios@bitbucket.org/marktimothytabios/accountservice.git
    ```

2. go to the directory

    ```
    cd accountservice
    ```

## Deployment

1. Bring up the application. This will automatically be tailing the logs.

    ```
    docker-compose up
    ```

You should first see the database starting (postgres) then the application
![Application Start up](src/docs/applicationStart.png "Optional title")

## Running Tests

While the application is running, you may now perform tests.

1. Swagger
    * From your browser, go to the link
    * `http://localhost:8090/accounservice/swagger-ui.html`
    ![API Swagger](src/docs/swaggerui.png "Optional title")

2. Postman
    * Import the file from the project named `AccountService.postman_collection.json`
    * Then your post man should have the API request and response
    ![API Swagger](src/docs/postman.png "Optional title")


    

