package com.example.accountservice.utility;

/**
 * This is a utility class to provide Expense calculations.
 */
public class ExpenseCalculator {

    /**
     * This will calculate that the earnings against the allowable amount.
     * @param allowedAmount
     * @param salary
     * @param expense
     *
     * @return
     */
    public static Boolean isLessThanAllowedAmount(Double allowedAmount, Double salary, Double expense) {
        return allowedAmount > (salary - expense);
    }
}
