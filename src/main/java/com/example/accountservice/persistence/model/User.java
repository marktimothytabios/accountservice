package com.example.accountservice.persistence.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "Users")
@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String emailAddress;
    private Double salary;
    private Double expenses;

}
