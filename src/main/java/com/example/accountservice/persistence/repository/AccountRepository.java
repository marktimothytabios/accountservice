package com.example.accountservice.persistence.repository;

import com.example.accountservice.persistence.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {

}
