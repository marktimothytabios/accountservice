package com.example.accountservice.rest.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.*;
import java.io.Serializable;

@ApiModel(description = "Request Object for the user.")
@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class UserRequest implements Serializable {

    @ApiModelProperty(notes = "The unique id of the user.",
        accessMode = ApiModelProperty.AccessMode.READ_ONLY, example = "1231231231")
    private Long id;

    @ApiModelProperty(notes = "The name of the user.",
            required = true, example = "Mark Tabios")
    @NotBlank
    @Size(max = 50)
    private String name;

    @ApiModelProperty(notes = "The email address of the user.",
            required = true, example = "marktimothytabios@yahoo.com")
    @NotNull
    @Email
    @Size(max = 100)
    private String emailAddress;

    @ApiModelProperty(notes = "The monthly salary of the user.",
            required = true, example = "10000.00")
    @NotNull
    @DecimalMin("0.01")
    @Digits(integer = 10, fraction = 2)
    private Double salary;

    @ApiModelProperty(notes = "The monthly expense of the user.",
            required = true, example = "5000.00")
    @NotNull
    @DecimalMin("0.01")
    @Digits(integer = 10, fraction = 2)
    private Double expenses;

}
