package com.example.accountservice.rest.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data @Builder @NoArgsConstructor @AllArgsConstructor
@ApiModel(description = "Account Request for Create.")
public class AccountRequest implements Serializable {

    @NotNull
    @Min(0)
    @Digits(integer = 10, fraction = 0)
    @ApiModelProperty(notes = "The user id.", required = true)
    private Long userId;
}
