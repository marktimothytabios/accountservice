package com.example.accountservice.rest.constants;

public enum Response {
    REQUEST_VALIDATION_FAILED("request_validation_failed", "The request was provided contains invalid values.", "Please provide a valid value for the field"),
    USER_NOT_FOUND("user_not_found", "The user provided was not found.", "Please provide a valid user."),
    INSUFFICIENT_EARNINGS("insufficient_earnings", "The monthly earnings is insufficient.", "Please ensure the user has enough income."),
    EMAIL_USED_UNAVAILABLE("email_used_unavailable", "The email provided is already taken.", "Please provide a different email address."),
    UNEXPECTED_SYSTEM_FAILURE("system_failure", "Unexpected System Failure while processing your request.", "Please contact your administrator");

    private String errorCode;
    private String errorText;
    private String errorCorrection;

    Response(String errorCode, String errorText, String errorCorrection) {
        this.errorCode = errorCode;
        this.errorText = errorText;
        this.errorCorrection = errorCorrection;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public String getErrorText() {
        return this.errorText;
    }

    public String getErrorCorrection() {
        return errorCorrection;
    }
}
