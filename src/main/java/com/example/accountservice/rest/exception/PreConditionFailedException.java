package com.example.accountservice.rest.exception;

import com.example.accountservice.rest.constants.Response;
import org.springframework.http.HttpStatus;

public class PreConditionFailedException extends AccountServiceException {

    public PreConditionFailedException(Response status) {
        this(status, status.getErrorText());
    }

    public PreConditionFailedException(Response status, String message) {
        this(message);
        this.setStatus(status);
    }

    public PreConditionFailedException(String message) {
        super(message);
        this.setCode(HttpStatus.PRECONDITION_FAILED);
    }
}
