package com.example.accountservice.rest.exception;

import com.example.accountservice.rest.constants.Response;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice
public class RestExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

    /**
     * Handles request level vaidation.
     *
     * @param httpRes
     * @param e
     * @return
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseBody
    public ErrorResponse requestValidationError(HttpServletResponse httpRes, MethodArgumentNotValidException e) {
        BindingResult result = e.getBindingResult();
        final StringBuilder sb = new StringBuilder();
        sb.append("Validation failed: ");
        result.getFieldErrors().forEach(err -> {
            sb.append(String.format("[%1$s [%2$s] %3$s] ", err.getField(), err.getRejectedValue(),
                    err.getDefaultMessage()));
        });
        logger.info(sb.toString(), e);
        httpRes.setStatus(HttpStatus.BAD_REQUEST.value());
        return new ErrorResponse(HttpStatus.BAD_REQUEST.value(),
                Response.REQUEST_VALIDATION_FAILED,
                sb.toString());
    }

    /**
     * Handles Account Service Exception.
     *
     * @param httpServletResponse
     * @param e
     * @return
     */
    @ExceptionHandler(value = AccountServiceException.class)
    @ResponseBody
    public ErrorResponse preConditionFailed(HttpServletResponse httpServletResponse, AccountServiceException e) {
        logger.warn(e.getMessage(), e);
        httpServletResponse.setStatus(e.getCode().value());
        return new ErrorResponse(e.getCode().value(), e.getStatus());
    }

    /**
     * Unknown exception handler.
     *
     * @param httpRes
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ErrorResponse unknownError(HttpServletResponse httpRes, Exception e) {
        logger.warn(e.getMessage(), e);
        httpRes.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                Response.UNEXPECTED_SYSTEM_FAILURE);
    }


    /**
     * The Error Response block when an exception happens.
     */
    @Getter @Setter
    public static class ErrorResponse {
        private int statusCode;
        private String errorCode;
        private String errorText;
        private String errorDescription;

        public ErrorResponse(int statusCode, Response status) {
            super();
            this.statusCode = statusCode;
            this.errorCode = status.getErrorCode();
            this.errorText = status.getErrorText();
            this.errorDescription = status.getErrorCorrection();
        }

        public ErrorResponse(int statusCode, Response status, String errorText) {
            super();
            this.statusCode = statusCode;
            this.errorCode = status.getErrorCode();
            this.errorText = errorText;
            this.errorDescription = status.getErrorCorrection();
        }
    }

}