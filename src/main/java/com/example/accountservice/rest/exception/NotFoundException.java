package com.example.accountservice.rest.exception;

import com.example.accountservice.rest.constants.Response;
import org.springframework.http.HttpStatus;

public class NotFoundException extends AccountServiceException {

    public NotFoundException(Response status) {
        this(status, status.getErrorText());
    }

    public NotFoundException(Response status, String message) {
        this(message);
        this.setStatus(status);
    }

    public NotFoundException(String message) {
        super(message);
        this.setCode(HttpStatus.NOT_FOUND);
    }
}
