package com.example.accountservice.rest.exception;

import com.example.accountservice.rest.constants.Response;
import org.springframework.http.HttpStatus;

public class AccountServiceException extends RuntimeException {

    private HttpStatus code = HttpStatus.INTERNAL_SERVER_ERROR;
    private Response status = Response.UNEXPECTED_SYSTEM_FAILURE;

    public AccountServiceException(Response status) {
        this(status, status.getErrorText());
    }

    public AccountServiceException(Response status, String message) {
        this(message);
        this.status = status;
    }

    public AccountServiceException(String message) {
        super(message);
    }

    public HttpStatus getCode() {
        return code;
    }

    public void setCode(HttpStatus code) {
        this.code = code;
    }

    public Response getStatus() {
        return status;
    }

    public void setStatus(Response status) {
        this.status = status;
    }
}
