package com.example.accountservice.rest.controllers;

import com.example.accountservice.persistence.model.User;
import com.example.accountservice.rest.constants.AccountServiceApiRestController;
import com.example.accountservice.rest.model.UserRequest;
import com.example.accountservice.service.UserService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 *  user related endpoints
 */
@AccountServiceApiRestController
@Api(value = "User Management System", description = "Operations pertaining to User Management System.")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * This will create the user.
     * @param userReq
     * @return
     */
    @ApiOperation(value = "Creates a new user", response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The user was successfully created."),
            @ApiResponse(code = 400, message = "Request Validation Error."),
            @ApiResponse(code = 412, message = "Email provided is already in the System."),
            @ApiResponse(code = 500, message = "Unexpected System Failure")
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/users", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public User createUser(@RequestBody @Valid final UserRequest userReq) {
        return userService.createUser(userReq);
    }

    /**
     * This will get all the users.
     *
     * @return
     */
    @ApiOperation(value = "List All Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retrieve all the users."),
            @ApiResponse(code = 500, message = "Unexpected System Failure")
    })
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/users", produces = APPLICATION_JSON_VALUE)
    public List<User> getUsers() {
        return userService.listUsers();
    }

    /**
     * This will get the user details of the user.
     *
     * @param userId
     * @return
     */
    @ApiOperation(value = "View the User", response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retrieve the user."),
            @ApiResponse(code = 404, message = "User not found."),
            @ApiResponse(code = 500, message = "Unexpected System Failure")
    })
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/users/{id}", produces = APPLICATION_JSON_VALUE)
    public User getUser(@ApiParam(
            value = "userId from the User that will be retrieved",
            required = true,
            example = "123123123") @PathVariable("id") Long id) {
        return userService.getUser(id);
    }

}
