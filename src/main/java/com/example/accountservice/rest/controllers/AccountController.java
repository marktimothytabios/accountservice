package com.example.accountservice.rest.controllers;

import com.example.accountservice.persistence.model.Account;
import com.example.accountservice.rest.constants.AccountServiceApiRestController;
import com.example.accountservice.rest.model.AccountRequest;
import com.example.accountservice.service.AccountService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@AccountServiceApiRestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    /**
     * This will orchestrate the account creation.
     *
     * @param request
     * @return
     */
    @ApiOperation(value = "Creates a new account", response = Account.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The account was successfully created."),
            @ApiResponse(code = 400, message = "Request Validation Error."),
            @ApiResponse(code = 404, message = "User Provided does not exist."),
            @ApiResponse(code = 412, message = "Insufficient Earnings."),
            @ApiResponse(code = 500, message = "Unexpected System Failure")
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/accounts", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public Account create(@RequestBody @Valid final AccountRequest request) {
        return accountService.create(request.getUserId());
    }

    /**
     * This will list all the account.
     *
     * @return List
     */
    @ApiOperation(value = "Creates a new account", response = Account.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "View all the accounts."),
            @ApiResponse(code = 500, message = "Unexpected System Failure")
    })
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/accounts", produces = APPLICATION_JSON_VALUE)
    public List<Account> list() {
        List<Account> accounts = accountService.listAll();
        return accounts;
    }
}
