package com.example.accountservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ListenerEvent {

    private static final Logger logger = LoggerFactory.getLogger(ListenerEvent.class);

    @EventListener
    public void handleEvent(Object event) {
        logger.debug("Event: [{}]", event);
    }
}
