package com.example.accountservice.service;

import com.example.accountservice.persistence.model.User;
import com.example.accountservice.rest.model.UserRequest;

import java.util.List;

/**
 * Perform user related services
 */
public interface UserService {

    /**
     * Creates a user.
     *
     * @param req
     * @return
     */
    User createUser(UserRequest req);

    /**
     * Lists all the users.
     *
     * @return
     */
    List<User> listUsers();

    /**
     * Gets a user.
     *
     * @param id
     * @return
     */
    User getUser(Long id);
}
