package com.example.accountservice.service;

import com.example.accountservice.persistence.model.Account;
import com.example.accountservice.persistence.model.User;
import com.example.accountservice.persistence.repository.AccountRepository;
import com.example.accountservice.persistence.repository.UserRepository;
import com.example.accountservice.rest.constants.Response;
import com.example.accountservice.rest.exception.NotFoundException;
import com.example.accountservice.rest.exception.PreConditionFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static com.example.accountservice.utility.ExpenseCalculator.isLessThanAllowedAmount;

@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountProperties propertiesService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Account create(Long id) {
        // find user and throw and error if not found.
        final User user = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(Response.USER_NOT_FOUND));

        // Earnings should be more than the configured amount.
        final Boolean accountAllowedCreationAmount = isLessThanAllowedAmount(Double.valueOf(propertiesService
                .getAllowedCreationEarnings()), user.getSalary(), user.getExpenses());

        if (accountAllowedCreationAmount) {
            throw new PreConditionFailedException(Response.INSUFFICIENT_EARNINGS);
        }

        // Save the account.
        final Account account = accountRepository.saveAndFlush(Account.builder()
                .user(user)
                .createDate(new Date())
                .build());

        // Return the mapped account.
        return account;
    }

    @Override
    public List<Account> listAll() {
        return accountRepository.findAll();
    }
}
