package com.example.accountservice.service;

import com.example.accountservice.persistence.model.Account;

import java.util.List;

/**
 * Performs Account related services.
 */
public interface AccountService {

    Account create(Long id);

    List<Account> listAll();

}
