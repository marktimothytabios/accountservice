package com.example.accountservice.service;

import com.example.accountservice.persistence.model.User;
import com.example.accountservice.persistence.repository.UserRepository;
import com.example.accountservice.rest.constants.Response;
import com.example.accountservice.rest.exception.NotFoundException;
import com.example.accountservice.rest.exception.PreConditionFailedException;
import com.example.accountservice.rest.model.UserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User createUser(UserRequest req) {
        // validate that email provided does not exist in the system.
        userRepository.findByEmailAddress(req.getEmailAddress())
                .ifPresent(u -> {
                    throw new PreConditionFailedException(Response.EMAIL_USED_UNAVAILABLE);
                });

        // save and return the user.
        return userRepository
                .saveAndFlush(User.builder()
                        .emailAddress(req.getEmailAddress())
                        .expenses(req.getExpenses())
                        .name(req.getName())
                        .salary(req.getSalary())
                        .build());
    }

    @Override
    public List<User> listUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUser(Long id) {
        // find user and throw and error if not found.
        final User user = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(Response.USER_NOT_FOUND));

        // Return the user mapped in the UserRequest
        return user;
    }
}
