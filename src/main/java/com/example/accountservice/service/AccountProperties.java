package com.example.accountservice.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class AccountProperties {

    @Value("${account.allowed.creation.earning:1000}")
    private String allowedCreationEarnings;

    /**
     * @return allowedCreationEarnings.
     */
    public String getAllowedCreationEarnings() {
        return allowedCreationEarnings;
    }
}
