CREATE TABLE Users (
    id              BIGINT          GENERATED ALWAYS AS IDENTITY (START WITH 10000 INCREMENT BY 1),
    name            VARCHAR(250)    NOT NULL,
    emailAddress    VARCHAR(250)    NOT NULL,
    salary          NUMERIC(10, 2)  NOT NULL,
    expenses        NUMERIC(10, 2)  NOT NULL,
    CONSTRAINT pk_users_id PRIMARY KEY(id)
);

CREATE TABLE Accounts(
    id              BIGINT          GENERATED ALWAYS AS IDENTITY (START WITH 100000000 INCREMENT BY 1),
    userId          BIGINT          NOT NULL,
    createDate      TIMESTAMP       NOT NULL,
    CONSTRAINT pk_accounts_id PRIMARY KEY(id),
    CONSTRAINT fk_accounts_user FOREIGN KEY (userId) REFERENCES Users(id)
);