package com.example.accountservice.rest.controllers;

import com.example.accountservice.IntegrationTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

public class AccountControllerTest extends IntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void getAccounts() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/accounts")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].user.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].createDate").isNotEmpty())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}