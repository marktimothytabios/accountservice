package com.example.accountservice.rest.controllers;

import com.example.accountservice.IntegrationTest;
import com.example.accountservice.rest.model.UserRequest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * This will test the UserController.
 */
public class UserControllerTest extends IntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void getUsers() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/users")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].name").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].emailAddress").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].expenses").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].salary").isNotEmpty())
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void getUser() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/users/10000")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(10000))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Mark Tabios"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.emailAddress").value("marktimothytabios@yahoo.com"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(10000))
                .andExpect(MockMvcResultMatchers.jsonPath("$.expenses").value(2555.55))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void createUser() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/api/v1/users")
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(UserRequest.builder()
                        .expenses(Double.valueOf("1000.00"))
                        .salary(Double.valueOf("100000.00"))
                        .name("John Doe")
                        .emailAddress("johnDoe@test.com")
                        .build()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

}